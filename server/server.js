const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const config = require("./config.json");
const Database = require("./database");
const DB = new Database(config.DB);

DB.on("error", err => {
    console.log("[-]".red + " Не удалось подключиться к базе данных");
    console.log(err);
});

const cache = {};


require("colors");




if (!cache.usersWriting) cache.usersWriting = [];

const   server = {
            online: 0
        };

async function main() {
    
    console.log("[#]".green + " Запуск сервера");

    console.log("[#]".green + " Загрузка базы данных");
    await DB.connect();
    console.log("[+] ".green + `База данных подключена`);


    io = require("socket.io")(config.PORT);

    io.on('connection', function (socket) {

        socket.on("auth", async (login, password) => {
    
            let [user, created] = await DB.Users.findOrCreate({
                where: { login },
                defaults: { password: await hashEncrypt(password) }
            });

            if (!created) {
                if (!(await hashCompare(password, user.password))) return socket.emit("auth", {fail:true});
            } else {
                console.log(`----------------------`);
                console.log(`[+]`.green + `Создан новый аккаунт`);
                console.log(`User(${user.id}): ${user.login}`);
            }
    
            socket.emit("auth", {
                token: jwt.sign({id:user.id}, config.SECRET),
                fail: false
            });
    
        });
    
        socket.on("createAccount", async (login, password) => {
            try {

                let [user, created] = await DB.Users.findOrCreate({
                    where: { login },
                    defaults: { password: await hashEncrypt(password) }
                });

                if (created) {
                    console.log(`----------------------`);
                    console.log(`[+]`.green + `Создан новый аккаунт`);
                    console.log(`User(${user.id}): ${user.login}`);
                }
    
                socket.emit("createAccount", {
                    created
                });

            } catch (error) {

                socket.emit("createAccount", {
                    created: false
                });
                return console.log(err);

            }
    
        });

        async function resolveMessage(data) {
            return {
                content: data.content,
                userID: data.user,
                messageID: data.id,
                user: (await DB.Users.findByPk(data.user)).login
            }
        }

        async function getMessages() {
            let messages = await DB.Messages.findAll({ limit: 100 });
            let resolvedMessages = [];
            for (const message of messages) {
                resolvedMessages.push(await resolveMessage(message));                
            }
            return resolvedMessages;
        }
    
        socket.on("ready", async (token) => {
    
            let verifyUser = await new Promise((res, rej) => {
                jwt.verify(token, config.SECRET, (err, payload) => {
                    if (err) res(null)
                    else res(payload);
                });
            });
            if (!verifyUser) return socket.emit("ready", {
                connected: false
            });

            server.online++;
            let user = await DB.Users.findByPk(verifyUser.id);

            socket.broadcast.emit('userConnected', user.login); 
            console.log(`[+] `.green + `Пользователь ${user.login} присоединился к чату!\n` + `[$] `.yellow + `Онлайн: ${server.online}`); 
        
            socket.on("messageCreate", async content => {
                try {

                    let message = await DB.Messages.create({
                        content, 
                        user: user.id
                    });
                    console.log(`----------------------`);
                    console.log('User: ' + user.login + ' | Message: ' + message.content);
                    console.log(`[#] `.cyan + '====> Отправка сообщения всем клиентам');
                    socket.broadcast.emit('messageCreate', {...(await resolveMessage(message)), sended: true});
                } catch (error) {
                    console.log(error);
                    socket.emit("messageCreate", { sended: false });
                }
            });

            socket.on("userWriting", userID => {
                socket.broadcast.emit("userWriting", userID);
            });

            socket.on("userWritingStop", userID => {
                socket.broadcast.emit("userWritingStop", userID);
            });
        
            socket.on("disconnect", () => {
                server.online--;
                io.sockets.emit("userDisconnected", user.login);
                // socket.removeAllListeners(); ???
                console.log(`[-] `.red + `Пользователь ${user.login} отключился!`);
            });
            
            socket.on("userDisconnected", () => {
                
            });

            socket.on("userInfo", async id => {
                try {
                    let user = await DB.Users.findByPk(id);
                    if (!user) throw new Error("User not found");
                    socket.emit("userInfo", false, {
                        login: user.login,
                        created: user.createdAt
                    });
                } catch (error) {
                    socket.emit("userInfo", true);
                }
                
            });
    
            socket.emit("ready", {
                online: server.online,
                messages: await getMessages(),
                connected: true,
                id: user.id,
                login: user.login
            });
        });
        
    });
    
    console.log("[!] ".green + "Сервер запущен | PORT "+config.PORT);
}

main();


async function hashEncrypt(password) {
    return await bcrypt.hash(password, (await bcrypt.genSalt(10))+config.GLOBAL_SLAT);
}
async function hashCompare(password, hash) {
    return await bcrypt.compare(password, hash)
}