const { Sequelize } = require('sequelize');
const Events = require("events");
const fs = require("fs-extra");
const { resolve } = require('path');

module.exports = class extends Events {
    constructor(URI) {
        super();
        this.sequelize = new Sequelize(URI, {
            dialect: "sqlite",
            logging: false
        });
    }

    async connect() {
        try {

            await this.sequelize.authenticate();
            let files_models = fs.readdirSync("./models");
            
            for (const file of files_models) {
                let model = require(resolve(__dirname, "./models", file))(this.sequelize);
                
                await model.sync({alter:true});
                this[model.name] = model;
            }


            this.emit("ready", true);

        } catch (err) {
            this.emit("error", err);
            process.exit();
        }
    }
}