const { Model, DataTypes } = require("sequelize");

class Users extends Model {

}

module.exports = sequelize => {
    return Users.init({    
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        login: {
            type: DataTypes.STRING,
            validate:{
                len: [2, 26]
            },
            allowNull:false
        },
        password: {
            type: DataTypes.STRING,
            validate:{
                min: 10
            },
            allowNull:false
        }
    }, {
        sequelize
    });
}
