const { Model, DataTypes } = require("sequelize");

class Messages extends Model {

}

module.exports = sequelize => {
    return Messages.init({
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        content: {
            type: DataTypes.STRING,
            validate: {
                max: 2048
            },
            allowNull:false
        },
        user: {
            type: DataTypes.BIGINT
        }
    }, {
        sequelize
    });
};